/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hmv.rmi.control;

import hmv.rmi.Interface.RMILoginInterface;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import hmv.rmi.model.User;
import hmv.rmi.view.RMILoginServerView;
import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hmv
 */
public class RMILoginServerControl extends UnicastRemoteObject implements RMILoginInterface {

    
    private RMILoginServerView view;

    public RMILoginServerControl(RMILoginServerView view) throws RemoteException {
        this.view = view;
        //getDBConnection("wtrkvbkm_java", "wtrkvbkm_java", "qrFkrlfsu");
        view.showMessage("RMI server is running...");
        try {
            view = new RMILoginServerView();
            LocateRegistry.createRegistry(1997);
            String res = "rmi://localhost:1997/rmiLogin";
            Naming.rebind(res, this);
        } catch (Exception e) {
        }

    }

    public String checkLogin(User user) throws RemoteException {
        String result = "";
        if (checkUser(user)) {
            result = "ok";
        }
        return result;

    }
    public boolean checkUser (User user){
        String username= "hungmv";
        String password = "hmv123";
        if(user.getUserName().equals(username) && user.getPassword().equals(password)){
            return true;
        }
        else return false;
    }
//    private void getDBConnection(String dbName, String username, String password) {
//        String dbUrl = "jdbc:mysql://hungmv.tech:3306/" + dbName;
//        String dbClass = "com.mysql.jdbc.Driver";
//        try {
//            Class.forName(dbClass);
//            con = DriverManager.getConnection(dbUrl, username, password);
//        } catch (Exception e) {
//            view.showMessage(e.getStackTrace().toString());
//        }
//    }
//
//    private boolean checkUser(User user) {
//        String query = "Select * FROM users WHERE username ='" + user.getUserName() + "' AND password ='" + user.getPassword() + "'";
//        try {
//            Statement stmt = con.createStatement();
//            ResultSet rs = stmt.executeQuery(query);
//            if (rs.next()) {
//                return true;
//            }
//        } catch (Exception e) {
//            view.showMessage(e.getStackTrace().toString());
//        }
//        return false;
//    }
}
