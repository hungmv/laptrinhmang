/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hmv.rmi.control;

import hmv.rmi.view.RMILoginServerView;

/**
 *
 * @author hmv
 */
public class ServerRun {

    public static void main(String[] args) {
        RMILoginServerView view = new RMILoginServerView();
        try {
            RMILoginServerControl control = new RMILoginServerControl(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
