/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rmi.Interface;
import java.rmi.Remote;
import java.rmi.RemoteException;
import com.rmi.model.User;

/**
 *
 * @author hmv
 */
public interface RMILoginInterface extends Remote {

    public String checkLogin(User user) throws RemoteException;
}
