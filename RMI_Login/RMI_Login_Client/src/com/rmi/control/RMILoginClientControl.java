/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rmi.control;

import com.rmi.Interface.RMILoginInterface;
import com.rmi.model.User;
import com.rmi.view.RMILoginClientView;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.NotBoundException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.rmi.Remote;
import java.rmi.Naming;

/**
 *
 * @author hmv
 */
public class RMILoginClientControl {

    private RMILoginClientView view;
    private RMILoginInterface rmiServer;


    public RMILoginClientControl(RMILoginClientView view) {
        this.view = view;
        view.addLoginListener(new LoginListener());
        try {
           String res = "rmi://localhost:1997/rmiLogin";
           Remote remoteService = Naming.lookup(res);
           rmiServer = (RMILoginInterface)remoteService;
           view.addLoginListener(new LoginListener());
        } catch (Exception e) {
        }
    }
    
    class LoginListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                User user = view.getUser();
                if (rmiServer.checkLogin(user).equals("ok")) {
                    view.showMessage("Loginsuccesfully!");
                } else {
                    view.showMessage("Invalid usernameand/orpassword!");
                }
            } catch (Exception ex) {
                view.showMessage(ex.getStackTrace().toString());
                ex.printStackTrace();
            }
        }
    }
}


