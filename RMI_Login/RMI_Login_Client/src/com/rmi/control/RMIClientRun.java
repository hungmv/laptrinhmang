/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rmi.control;

import com.rmi.view.RMILoginClientView;

/**
 *
 * @author hmv
 */
public class RMIClientRun {

    public static void main(String[] args) {
        RMILoginClientView view = new RMILoginClientView();
        RMILoginClientControl control = new RMILoginClientControl(view);
        view.setVisible(true);
    }
}
