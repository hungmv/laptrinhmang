/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hmv.rmi.Interface;

import hmv.rmi.model.User;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author hmv
 */
public interface RMI_Cal extends Remote {

    public String reverse(String str) throws RemoteException;
    public int USCLN(int a, int b) throws RemoteException;
    public String checkLogin(User user) throws RemoteException;

}
