/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hmv.rmi.model;

/**
 *
 * @author hmv
 */
public class ReverseString {

    private String _string;

    public ReverseString() {

    }

    public ReverseString(String _string) {
        this._string = _string;
    }

    public String getString() {
        return _string;
    }

    public void setString(String _string) {
        this._string = _string;
    }

    public void reverse() {
        String tmp = "";
        for (int i = _string.length() - 1; i >= 0; i--) {
            tmp += _string.substring(i, i + 1);
        }
        this._string = tmp;
    }
}
