/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hmv.rmi.control;

import hmv.rmi.Interface.RMI_Cal;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;


/**
 *
 * @author hmv
 */
public class ServerRun {

    public static void main(String[] args) throws RemoteException {
        try {
            RMI_Cal rmiCal = new RMI_Impl();
            
            String url = "rmi://localhost:1997/rmiCal";
            Naming.rebind(url, rmiCal);
            System.out.println(">>>>INFO: RMI Server Running!!");
        } catch (Exception e) {
        }
    }
}
