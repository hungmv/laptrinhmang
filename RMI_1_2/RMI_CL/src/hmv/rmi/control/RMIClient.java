/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hmv.rmi.control;

import hmv.rmi.Interface.RMI_Cal;
import hmv.rmi.model.USCLNMol;
import hmv.rmi.view.USCLN_Form;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author hmv
 */
public class RMIClient {
    private USCLN_Form uscView;
    private RMI_Cal rmiServer;
    class LoginListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                USCLNMol uscln = uscView.getUSCLN();
                int kq = rmiServer.USCLN(uscln);
                uscView.setResult(Integer.toString(kq));
            } catch (Exception ex) {
                uscView.showMessage(ex.getStackTrace().toString());
                ex.printStackTrace();
            }
        }
    }


}   
